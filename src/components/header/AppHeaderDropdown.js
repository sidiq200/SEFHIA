import React from 'react'
import {
  CAvatar,
  CBadge,
  CDropdown,
  CDropdownDivider,
  CDropdownHeader,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  cilBell,
  cilCreditCard,
  cilCommentSquare,
  cilEnvelopeOpen,
  cilLockLocked,
  cilSettings,
  cilTask,
  cilUser,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'

import avatar8 from './../../assets/images/avatars/8.jpg'

const AppHeaderDropdown = () => {
  return (
    <CDropdown variant="nav-item">
      <CDropdownToggle placement="bottom-end" className="py-0" caret={false}>
        <CAvatar src={avatar8} size="md" />
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownHeader className="bg-light fw-semibold py-2">Account</CDropdownHeader>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />

        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />
 
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />

        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />

        </CDropdownItem>
        <CDropdownHeader className="bg-light fw-semibold py-2">Settings</CDropdownHeader>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />
          
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />
          
        </CDropdownItem>
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />
          
          <CBadge color="secondary" className="ms-2">
           
          </CBadge>
        </CDropdownItem>

        <CDropdownDivider />
        <CDropdownItem href="#">
          <CIcon icon={""} className="me-2" />
          
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  )
}

export default AppHeaderDropdown
